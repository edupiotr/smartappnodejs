'use strict';

require('dotenv').config(); 
const express = require('express');
const session = require("express-session");
const path = require('path');
const morgan = require('morgan');
const encodeUrl = require('encodeurl');
const rp = require('request-promise-native');
const SmartApp = require('@smartthings/smartapp');

const port = process.env.PORT || 3000
const serverUrl = process.env.SERVER_URL || 'http://localhost:3000'
const redirectUri = `${serverUrl}/oauth/callback`;
const scope = encodeUrl('r:locations:* r:scenes:* x:scenes:* r:devices:* x:devices:*');
const appId = process.env.APP_ID
const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;



// let localStore = {
// 	installedAppId: "6809802a-7bba-4f9d-ad7b-cd3aff82dca4",
//     locationId: "4aaf0066-acb3-46c1-b525-9a0d2e7f43d8",
//     authToken: "eeaab697-d68c-427b-948c-d57698037712",
//     refreshToken: "dad16458-f06a-4440-adba-25ab861367d3"
// } /* This will be fetched from db */

// let localStore = {
//     authToken: "0fa55c21-e375-4168-ab78-cd1011e269de",
//     refreshToken: "0fa55c21-e375-4168-ab78-cd1011e269de"
// } 

// let localStore = null;

let contextStore = null;
let contextData = null;

/* SmartThings API */
//APP_ID=ac609211-d6b7-4225-9dde-d7709fe76fb2
const smartApp = new SmartApp()
	.appId(appId)
	.clientId(clientId)
	.clientSecret(clientSecret)
	.redirectUri(redirectUri)

/* Webserver setup */
const server = express();
// server.set('views', path.join(__dirname, 'views'));
// server.set('view engine', 'ejs');
// server.use(morgan('dev'));
server.use(express.json());
// server.use(express.urlencoded({extended: false}));
// server.use(session({
// 	secret: "oauth example secret",
// 	resave: false,
// 	saveUninitialized: true,
// 	cookie: {secure: false}
// }));
// server.use(express.static(path.join(__dirname, 'public')));

/* Main page. Shows link to SmartThings if not authenticated and list of scenes afterwards */
// server.get('/', function (req, res) {
// 	if (req.session.smartThings) {

// 		// Context cookie found, use it to list scenes
// 		const data = req.session.smartThings;
// 		smartApp.withContext(data).then(ctx => {
// 			ctx.api.scenes.list().then(scenes => {
// 				res.render('scenes', {
// 					installedAppId: data.installedAppId,
// 					locationName: data.locationName,
// 					errorMessage: '',
// 					scenes: scenes
// 				})
// 			}).catch(error => {
// 				res.render('scenes', {
// 					installedAppId: data.installedAppId,
// 					locationName: data.locationName,
// 					errorMessage: `${error.message}`,
// 					scenes: {items:[]}
// 				})
// 			})
// 		})
// 	}
// 	else {
// 		loadContext()
// 	}
// });

// async function loadContext() {
// 	console.log("Load context !");
// 	contextStore = await smartApp.withContext({
// 		// installedAppId: localStore.installedAppId,
// 		// locationId: localStore.locationId,
// 		authToken: localStore.authToken,
// 		refreshToken: localStore.refreshToken});
// }



// /* Executes a scene */
// server.post('/scenes/:sceneId', function (req, res) {
// 	smartApp.withContext(req.session.smartThings).then(ctx => {
// 		ctx.api.scenes.execute(req.params.sceneId).then(result => {
// 			res.send(result)
// 		})
// 	})
// });


// server.post('/', (req, res, next) => {
//     let evt = req.body;
//    let lifecycle = evt.lifecycle;
//    console.log("IVE GOT PHASE !!!!!! " + lifecycle);
//    console.log("WITH NICE BODY !!!! " + evt)
//     smartApp.handleHttpCallback(req, res);
// });



function showContext(ctx) {
	console.log("Context:");
	// console.log("installdAppId: " + ctx.installedAppId);
	// console.log("locationId: " + ctx.locationId);
	console.log("authToken: " + ctx.authToken);
	console.log("refreshToken: " + ctx.refreshToken);
};



server.get('/showContext', (req, res) => {
    console.log("Hello Mrs Roe");
	showContext(contextStore);
	res.json({
		installdAppId : contextStore.installedAppId,
		locationId : contextStore.locationId,
		authToken : contextStore.authToken,
		refreshToken : contextStore.refreshToken,
	});
});


///////////////////// DEFAULT SMARTAPP ENDPOINTS

/* Uninstalls app and clears context cookie */
server.get('/logout', async function(req, res) {
	const ctx = await smartApp.withContext(req.session.smartThings)
	await ctx.api.installedApps.delete()
	req.session.destroy(err => {
		res.redirect('/')
	})
});

/* Handles OAuth redirect */
server.get('/oauth/callback', async (req, res) => {
	console.log("Caallback request = " + req)
	// Exchange the code for the auth token. Returns an API context that can be used for subsequent calls
	const ctx = await smartApp.handleOAuthCallback(req)
	// console.log("Handled !")
	contextStore = ctx;
	// Get the location name
	const location = await ctx.api.locations.get();
	// console.log("Location = " + location);
	showContext(ctx)
	// Set the cookie with the context, including the location ID and name
	req.session.smartThings = {
		locationId: ctx.locationId,
		locationName: location.name,
		// installedAppId: ctx.installedAppId,
		authToken: ctx.authToken,
		refreshToken: ctx.refreshToken
	};
	// Redirect back to the main mage
	res.redirect('/')
});


///////////////////// FINAL ENDPOINTS
server.put('/loadContextDirectly', (req, res) => {
	// TODO: localstore prawdopodobnie do wywalenia;
	// trzeba chyba bedzie dorobic przeinstalowanie handlerow po zmianie tokena... albo i nie
	contextStore = createSmartAppContext(req)
	showContext(contextData)
	console.log("[Load context] AuthToken: %s RefreshToken: %s", req.body.authToken, req.body.refreshToken);
	res.send("New context has been initialized");
});

async function createSmartAppContext(req) {
	contextData = {
		installedAppId: "6809802a-7bba-4f9d-ad7b-cd3aff82dca4",
		authToken: req.body.authToken,
		refreshToken: req.body.refreshToken
	}
	contextStore = await smartApp.withContext({
		installedAppId: "6809802a-7bba-4f9d-ad7b-cd3aff82dca4",
		authToken: contextData.authToken,
		refreshToken: contextData.refreshToken
	});
	return contextStore;
}

server.put('/subscribe', async (req, res) => {
	// console.log("InstalledAppId = " + smartApp);
	// const deviceInfo = createDeviceInfo();
	// smartApp.updated(async (context, updateData) => {
	// 	console.log("SmartApp UPDATE proceeded");
		
		// Updated defines what code to run when the SmartApp is installed or the settings are updated by the user.
		
		//  // Clear any existing configuration.
		// await context.api.schedules.delete()
	
		
				// contextStore = context;
	// });
	
	smartApp.subscribedEventHandler('myDeviceEventHandler', async (context, event) => {		
		console.log('Sieeeeeeeemaaaaankoooooo !!!! capability=%s attribute=%s value=%s', event.capability, event.attribute, event.value);	
	});
	const deviceInfo = createDeviceInfo();
		await contextStore.api.subscriptions.subscribeToDevices(deviceInfo, 'switch', 'switch', 'myDeviceEventHandler');
	console.log("[Subscribe] Capability: %s Attribute: %s", req.body.capability, req.body.attribute);
	
	// await contextStore.api.subscriptions.delete();
	// await contextStore.api.subscriptions.subscribeToDevices(deviceInfo, req.body.capability, req.body.attribute, 'myDeviceEventHandler');
	res.send("Subscription has been made");
});


function createDeviceInfo() {
	let deviceRealId = "2df6f8ac-d8cb-4d98-bbaf-1bbd8373c57d";
	let componentId = "main";
	
	let deviceInfo=[{
		"valueType":"DEVICE",
		"deviceConfig":{
			"deviceId":deviceRealId,
			"componentId":componentId
		}
	}]
	return deviceInfo;
}

// async function createSmartAppContext() {
// 	return await smartApp.withContext({
// 		// installedAppId: localStore.installedAppId,
// 		// locationId: localStore.locationId,
// 		authToken: localStore.authToken,
// 		refreshToken: localStore.refreshToken
// 	});
// }

///////////////////// AUXILIARY ENDPOINTS
server.get('/showDevices', async (req, res, next) => {
	async function runAsync() {
		const devicesList = await getDevices();
		res.json(devicesList);
	}
	runAsync().catch(next);
});

async function getDevices() {
    const resul = await contextStore.api.devices.list();
	// console.log("Devs2 = " + JSON.stringify(resul));
	return resul;
};


server.listen(port);
console.log(`\nWebsite URL -- Use this URL to log into SmartThings and connect this app to your account:\n${serverUrl}\n`);
console.log(`Redirect URI -- Copy this value into the "Redirection URI(s)" field in the Developer Workspace:\n${redirectUri}`);


